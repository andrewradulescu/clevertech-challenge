import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HousesListComponent } from './houses-list.component';
import { NO_ERRORS_SCHEMA, DebugElement } from '@angular/core';
import { House } from '../../models';
import { By } from '@angular/platform-browser';

describe('HousesListComponent', () => {
    let component: HousesListComponent;
    let fixture: ComponentFixture<HousesListComponent>;
    const dummyHouses: House[] = [
        { id: 1, description: '', onSale: true, price: 100, title: 'House 1', image: '', cityId: 1 },
        { id: 2, description: '', onSale: true, price: 100, title: 'House 2', image: '', cityId: 2 }
    ];

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HousesListComponent],
            imports: [],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HousesListComponent);
        component = fixture.componentInstance;
        component.houses = dummyHouses;
        fixture.detectChanges();
    });

    // TODO(3pts)
    it('should render list of houses', () => {
        const el: DebugElement[] = fixture.debugElement.queryAll(By.css('app-house-card'));

        const displayedElements: number = el.reduce((x, y) => {
            return !!y.nativeElement ? ++x : x;
        }, 0);

        expect(displayedElements).toBe(dummyHouses.length);
    });
});
