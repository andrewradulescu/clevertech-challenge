import { Component, Input } from '@angular/core';
import { House } from '../../models';

/* TODO(5pts): render houses list */
@Component({
    selector: 'app-houses-list',
    template: `
     <ng-container *ngIf="houses.length > 0; else noHousesMatchingCriteria">
        <app-house-card *ngFor="let house of houses" [house]="house" routerLink="/real-estate/{{house.id}}"></app-house-card>
     </ng-container>

     <ng-template #noHousesMatchingCriteria>
        <p class="noHouses">Sorry, there are no houses matching the given criteria...</p>
     </ng-template>
  `,
    styles: [`
        :host { flex-wrap: wrap; display: flex; justify-content: center; }
        .noHouses { display: flex; justify-content: center; width: 100%; }
    `]
})
export class HousesListComponent {
    @Input() houses: House[];
}
