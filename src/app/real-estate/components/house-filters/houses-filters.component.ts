import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { City, HouseFilters } from '../../models';

/* TODO(5pts): create form controls */
/* TODO(5pts): render form */

@Component({
    selector: 'app-houses-filters',
    templateUrl: 'houses-filters.component.html',
    styles: [`

        .form-container {
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .form-container button {
            max-width: 10%;
        }

        .form-container > * {
            width: 80%;
        }

        .mat-radio-group {
            display: flex;
            flex-direction: column;
        }

  `]
})
export class HousesFiltersComponent {
    @Output() filtersChange = new EventEmitter<HouseFilters>();

    @Input()
    set filters(v: HouseFilters) {
        this.form.patchValue(v || {});
    }

    @Input() cities: City[];

    public form: FormGroup = new FormGroup({
        cityId: new FormControl(''),
        priceLessThan: new FormControl('', [Validators.min(0), Validators.max(Infinity)]),
        onSale: new FormControl('')
    });

    public isSearching: boolean;

    onSubmit(): void {
        const filtersObj = this.form.value;
        Object.keys(filtersObj).forEach((key) => (
            filtersObj[key] === null ||
            filtersObj[key] === '' ||
            filtersObj[key] === 'All'
        ) && delete filtersObj[key]);
        // Add some dummy loading animation
        this.mockedSearchTime();
        this.filtersChange.emit(filtersObj);
    }

    mockedSearchTime(): void {
        this.isSearching = true;
        setTimeout(() => this.isSearching = false, 2000);
    }

}
