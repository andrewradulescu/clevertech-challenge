import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormGroup } from '@angular/forms';
import { HousesFiltersComponent } from './houses-filters.component';
import { NO_ERRORS_SCHEMA, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('HousesFiltersComponent', () => {
    let component: HousesFiltersComponent;
    let fixture: ComponentFixture<HousesFiltersComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HousesFiltersComponent],
            imports: [],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HousesFiltersComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create filters form', () => {
        expect(component.form instanceof FormGroup).toBe(true);
    });

    // TODO(1pts)
    it('form should have cityId control', () => {
        expect(component.form.controls['cityId']).toBeDefined();
    });

    // TODO(1pts)
    it('should render cityId control', () => {
        const el: DebugElement = fixture.debugElement.query(By.css('[formControlName="cityId"]'));

        expect(el.nativeElement).not.toBe(null);
    });

    // TODO(1pts)
    it('form should have priceLessThan control', () => {
        expect(component.form.controls['priceLessThan']).toBeDefined();
    });

    // TODO(1pts)
    it('should render priceLessThan control', () => {
        const el: DebugElement = fixture.debugElement.query(By.css('[formControlName="priceLessThan"]'));

        expect(el.nativeElement).not.toBe(null);
    });

    // TODO(1pts)
    it('form should have onSale control', () => {
        expect(component.form.controls['onSale']).toBeDefined();
     });

    // TODO(1pts)
    it('should render onSale control', () => {
        const el: DebugElement = fixture.debugElement.query(By.css('[formControlName="onSale"]'));

        expect(el.nativeElement).not.toBe(null);
     });
});
