import { Component, Input } from '@angular/core';
import { House } from '../../models';

// TODO(5pts): render house

@Component({
    selector: 'app-house-card',
    templateUrl: 'house-card.component.html',
    styles: [`
        :host { display: flex; flex: 1 1 auto; padding: 1em; max-width: 400px; cursor: pointer; }

        .mat-card-title { font-weight: bold; }

        .mat-chip-list  { margin-left: auto; }
    `]
})
export class HouseCardComponent {
    @Input() house: House;
}
