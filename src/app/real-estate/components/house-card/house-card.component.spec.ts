import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HouseCardComponent } from './house-card.component';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { House } from '../../models';

describe('HouseCardComponent', () => {
    let component: HouseCardComponent;
    let fixture: ComponentFixture<HouseCardComponent>;
    const dummyHouse: House = {
        id: 1, cityId: 1, onSale: true, price: 100, title: 'House 1', description: 'Dummy desc', image: ''
    };

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HouseCardComponent],
            imports: [],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HouseCardComponent);
        component = fixture.componentInstance;
        component.house = dummyHouse;
        fixture.detectChanges();
    });

    // TODO(1pts)
    it('should render house title', () => {
        const el: DebugElement = fixture.debugElement.query(By.css('#title'));

        expect(el.nativeElement.innerHTML).toBe(dummyHouse.title);
    });

    // TODO(1pts)
    it('should render card desription', () => {
        const el: DebugElement = fixture.debugElement.query(By.css('#description'));

        expect(el.nativeElement.innerHTML).toBe(dummyHouse.description);
    });

    // TODO(1pts)
    it('should render card image', () => {
        const el: DebugElement = fixture.debugElement.query(By.css('img'));

        expect(el.nativeElement).not.toBe(null);
    });

    // TODO(1pts)
    it('should render house price', () => {
        const el: DebugElement = fixture.debugElement.query(By.css('#price'));

        expect(el.nativeElement.innerHTML).toContain(dummyHouse.price);
    });

    // TODO(1pts)
    it('should render house onSale if house is on sale', () => {
        component.house.onSale = true;
        fixture.detectChanges();

        const el: DebugElement = fixture.debugElement.query(By.css('#onSale'));

        expect(el.nativeElement).not.toBe(null);
    });

    // TODO(1pts)
    it('should NOT render house onSale if house is not on sale', () => {
        component.house.onSale = false;
        fixture.detectChanges();

        const el: DebugElement = fixture.debugElement.query(By.css('#onSale'));

        expect(el).toBeNull();
    });
});
