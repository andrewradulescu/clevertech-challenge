import { switchMap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { HousesService } from '../apis/houses.service';
import { House } from '../models';

@Component({
  selector: 'app-house-details',
  template: `
  <app-house-card [house]="house$ | async" data-test-house-card></app-house-card>
  `,
  styles: [
    `
      :host {
        display: flex;
        justify-content: center;
      }
    `
  ]
})
export class HouseDetailsComponent implements OnInit {
  house$: Observable<House>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private houseAPI: HousesService
  ) {}

  ngOnInit() {
    /* TODO(3pts)
      Goal: fetch house based on the activated route's :id
    */
   this.house$ = this.activatedRoute.paramMap
        .pipe(
            switchMap((params: ParamMap) => {
                const id = params.get('id');
                return id ? this.houseAPI.getHouse(+id) : [];
            })
        );
  }
}
