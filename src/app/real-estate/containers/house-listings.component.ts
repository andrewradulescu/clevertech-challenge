import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { CitiesService } from '../apis/cities.service';
import { HousesService } from '../apis/houses.service';
import { City, House, HouseFilters } from '../models';

@Component({
    selector: 'app-house-listings',
    template: `
    <app-houses-filters
      [cities]="cities$ | async"
      [filters]="filters$ | async"
      (filtersChange)="onFiltersChange($event)">
    </app-houses-filters>

    <app-houses-list
      [houses]="houses$ | async">
    </app-houses-list>
  `,
    styles: [`
    `
    ]
})
export class HouseListingsComponent implements OnInit {
    cities$: Observable<City[]>;
    filters$: Observable<HouseFilters>;
    houses$: Observable<House[]>;

    constructor(
        private citiesAPI: CitiesService,
        private houseAPI: HousesService,
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        /**
          1.TODO(1pts)
          Goal: fetch all cities
          Implementation: set this.cities$
        */
        this.cities$ = this.citiesAPI.getCities();

        /**
         2. TODO(5pts)
          Goal: parse query params stream into filters object
          Implementation: set this.filters$
          Hint:
            query params: real-estate?cityId=3&onSale=false&priceLessThan=244
            parsed value: { cityId: 3, onSale: false, priceLessThan: 244 }
        */
        // this.filters$ = this.activatedRoute.queryParams; // DEPRECATED but working for now, still need to preserve type
        this.filters$ = this.activatedRoute.queryParamMap
            .pipe(
                switchMap((params: ParamMap) => {
                    // Need to preserve type to keep Ramda's equals checking work and respect TS linter
                    const paramsObj: HouseFilters = {},
                        cityId = params.get('cityId'),
                        onSale = params.get('onSale'),
                        priceLessThan = params.get('priceLessThan');

                    if (cityId != null) {
                        paramsObj.cityId = +cityId;
                    }

                    if (onSale != null) {
                        paramsObj.onSale = (onSale === 'true');
                    }

                    if (priceLessThan != null) {
                        paramsObj.priceLessThan = +priceLessThan;
                    }

                    return of(paramsObj);
                })
            );

        /*
        3. TODO(8pts)
          Goal: fetch all houses matching current filters
          Implementation: set this.houses$
          Hint: this example includes using higher order observables.
                we must switch from the filters$ stream into the houses$ stream.
        */
        this.houses$ = this.filters$.pipe(
            switchMap((filters: HouseFilters) => {
                return this.houseAPI.getHouses(filters);
            })
        );
    }

    onFiltersChange(queryParams: HouseFilters) {
        /* TODO(1pts)
          Goal: update URL query params with the new filters
        */
        this.router.navigate([], { relativeTo: this.activatedRoute, queryParams: queryParams });
    }
}
